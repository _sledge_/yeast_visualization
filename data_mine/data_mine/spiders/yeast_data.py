import scrapy


class YeastSpider(scrapy.Spider):
    name = "strains"

    def start_requests(self):
        urls = [
            'https://www.whitelabs.com/yeast-bank?keywords=&flocculation=0&type=yeasts&temp_from=1&temp_to=100&atten_from=1&atten_to=100&tolerance=0&drink_type=0&yeast_type=0&op=SHOW+ALL+STRAINS&form_build_id=form-j9QB9z_z5soAcvsWPawgkIdm9Y4IVeowsa5W6gP2fW4&form_id=whitelabs_yeast_yeast_bank_form'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for yeasts in response.css('div.yeast-desc'):
            yield {
                'name': yeasts.css('h2::text').extract()
            }
        for yeasts in response.css('div.attenuation'):
            yield {
                'attenuation': yeasts.css('div.value::text').extract()
            }
        for yeasts in response.css('div.flocculation'):
            yield {
                'flocculation': yeasts.css('div.value::text').extract()
            }
        for yeasts in response.css('div.tolerance'):
            yield {
                'alcohol tolerance': yeasts.css('div.value::text').extract()
            }
        for yeasts in response.css('div.temperature'):
            yield {
                'temperature': yeasts.css('div.value::text').extract()
            }
                                        